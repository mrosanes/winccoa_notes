# winccoa_notes

## winccoa controls SW notes


Winccoa is a GUI used for creating supervision & control applications 

Key concepts:
- GEDI (wincc oa GUI) -> similar to Taurus (but with moveable synoptics)
- DataPoints (DP) in WinCCoa: representation of physical objects (valves, dampers, etc.) -> Similar to Tango Devices
- PARA -> similar to Jive
- Console (Servers can be run from there): Similar to Tango Astor
- Panel Topology: similar to Taurus panels (which panel can access another...)
- User permissions (visulization, control, etc.)
- DataPointType(DPT)->DataPoint(DP):   Similar to Instance->Class
- Master DP: acts as a crossover of DP type and DP. Masters are used to make DPs inherit the configs of the DPT.
- Dynamic Attributes: some of the attributes that are different for different DPs coming from the same DPT.
- Alerts Management: similar to Panic (alarms management) 